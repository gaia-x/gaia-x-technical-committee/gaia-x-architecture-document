# Annex

## Annex A - Gaia-X Digital Clearing House (GXDCH)

The Gaia-X Digital Clearing House (GXDCH) operationalizes the Gaia-X mission.
A GXDCH makes the various mechanisms and concepts applicable in practice as a ready-to-use service set.
Therefore, a distinct specification and implementation of the GXDCH exists.

The [Gaia-X Framework](http://docs.gaia-x.eu/framework/) contains functional and technical specifications, the Gaia-X Compliance Service to assess Gaia-X Compliance and the testbed to validate the behaviour of software components against the Gaia-X Policy Rules and technical specifications.

The GXDCH contains both mandatory and optional components. 

All the mandatory components of the GXDCH are open-source software.
The development and architecture of the GXDCH is under the governance of the Gaia-X Association.

A GXDCH instance runs the engine to validate the Gaia-X rules, therefore becoming the go-to place to become Gaia-X compliant.
The instances are non-exclusive, interchangeable, and operated by multiple market operators.  

!!! note

    It is crucial to differentiate **compliance** and **compatibility**.  
    A service can be made Gaia-X compliant with Gaia-X Policy Rules (see the Policy Rules Compliance Document). Software cannot.  
    However, software can be made Gaia-X compatible with Gaia-X specifications.

### Deployment 

Each GXDCH instance must be operated by a service provider according to rules defined with and approved by the Gaia-X Association.

Such providers have then the role of [Operator](gx_conceptual_model.md#operator).
Gaia-X is not an operator itself.

Any provider compliant with the requirements defined by the Gaia-X Association and featuring the necessary characteristics as defined by Gaia-X can become a GXDCH operator.

### Description of the GXDCH components

The GXDCH is open for future evolution. This means that the components included in the GXDCH may be enhanced from release to release.

#### Mandatory components

The components below are:

- technically compatible with the Gaia-X Specifications described in the functional and technical Specifications Documents of the [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
- validated by the Gaia-X Testbed, which will aim to verify the behaviour of the components.

Those services are provided by GXDCH operators only.

* The Gaia-X Compliance Service  
It validates the shape and content of the Gaia-X Credentials and, if successful, issues a `GaiaXComplianceCredential`.
* The Gaia-X Registry  
This service implements the [Gaia-X Registry](gx_services.md#gaia-x-registry) and represents the backbone of the Gaia-X Ecosystem governance.  
It stores Trust Anchors accredited by the Gaia-X Association, the shapes, and schemas that are used for the Gaia-X Compliance validation. Beyond these core functionalities, the Gaia-X Registry Service stores further accompanying information such as the terms and conditions for the Gaia-X Compliance Service.
* The Gaia-X Notarisation Services
For the business registration numbers it is a tool used to verify all registration numbers given by the participant in their Participant credentials.
* The Credential Event Service  
Provides a distributed storing solution to hold Gaia-X Compliant VCs and is used to synchronise the Credentials' IDs across the [Federated Catalogues](enabling_services.md#federated-catalogues) that want to publish Gaia-X conformant services.
* A [InterPlanetary File System (IPFS)](https://www.ipfs.tech/) node  
This is used to synchronise the information across GXDCH instances, especially the information exposed by the Gaia-X Registry.
* A Logging service  
This service is not necessarily exposed to customers. This is subject to the federator depending on the federator's applicable regulations.

#### Optional components

The following services are optional and can be provided by any Provider, which may or may not be a GXDCH operator.
Those services can also be operated by providers for specific Data Spaces or Federations.

* a Wizard  
UI to sign Verifiable Credentials in a Verifiable Presentation on the client side.
Calling the Gaia-X Compliance Service is integrated with the Wizard, making it possible to obtain Gaia-X Compliant Credentials by directly using this tool.
Depending on the implementation, a wizard can support signing with the hardware module via [PKCS#11](https://en.wikipedia.org/wiki/PKCS_11) or others like [Web eID](https://web-eid.eu/).
The Wizard also provides a user-friendly way to send the Gaia-X Compliant VCs to the Credential Events Service.
Finally, it provides an environment where the user can try out the Gaia-X Compliance for testing and learning purposes.
* a Wallet  
Application that allows the user to store their Credentials and present them to third parties when needed.
This is also sometimes called an Agent, CredentialRegistry or [IdentityHub](https://identity.foundation/decentralized-web-node/spec/0.0.1-predraft/).


* a Catalogue  
An instance of the Federated Catalogue using the Credential Event Service introduced in the previous section.
* a [Key Management System](https://en.wikipedia.org/wiki/Key_management#Key_management_system)  
This is provided by a provider as an additional service to help its customers to manage cryptographic material, including revocation, key rotation, key recovering, …
* Policy Decision Point (PDP)  
This service is used to perform the reasoning given one or more policy and input data.
The PDP can support several policy languages.
For each policy expressed in a Gaia-X Credential, the policy owner can give the list of PDP service endpoints, which can be used to compute the policies.
* Data Exchange services  
Those services are used to perform [Data Exchange](component_details.md#data-exchange-services).

The optional services provided may be subject to fees set by the provider.

### Gaia-X Digital Clearing House Releases

Each GXDCH release is a bundle of several components developed by the Gaia-X Lab with the support and contribution of the open-source community.

The components and their documentation are published for Operational and Lab versions at https://registry.gaia-x.eu/

#### Elbe release

This was an internal release only for testing purposes.

#### Tagus release

This is the ongoing development cycle under `/v1`.

In order to allow several instances of different versions to be accessible in parallel, we specify paths during the deployment.  
That means that `/development` uses the latest `development` image of a component, `/v1` uses the latest `v1` image, and `/main` uses the latest `main` image.

To ensure consistency, GXDCH operators are requested to use the same convention for their instances.

#### Loire release

This is the next major release developed by the Gaia-X Lab with the contribution of the open-source community.

## Gaia-X Digital Clearing House Operations

#### GXDCH usage

For the Tagus release, the Gaia-X Association provides a load balancer in the form of some HAProxy nodes pointing to Clearing House instances.
<!-- langec: quite a lot of technical detail for an Architecture Document -->These HAProxy nodes are managed through [Load Balancer Ansible scripts](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/README.md). 

The Gaia-X Association is responsible for these scripts and their update after a Clearing House is deployed.
GXDCH operators can open merge requests on the [HAProxy configuration](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/haproxy.cfg) to include their GXDCH instance.

The services are accessible behind three subdomains:

- https://registrationnumber.notary.gaia-x.eu/v1/docs/
- https://compliance.gaia-x.eu/v1/docs/
- https://registry.gaia-x.eu/v1/docs/

#### GXDCH setup

Technical details are available in the [setup guide](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/README.md).

## Annex B - Testbed

The testbed aims to verify the behaviour of a software component with regard to:

- Gaia-X Policy Rules
- Gaia-X Technical Specifications

The expected output of the testbed is a [Gaia-X Credential](operating_model.md#gaia-x-credentials-and-attestations) containing claims about:

- the binary signature of the tested software component - like [sigstore](https://www.sigstore.dev/)
- the version of the verified Gaia-X policy rules and technical specifications
- a [validity period](https://www.w3.org/TR/vc-data-model-2.0/#validity-period) - not too short to have market adoption and stability, not too long to reduce software version fragmentation.

#### for live service

The testbed can also be used to validate the behaviour of already deployed services.

This usage is left as an opportunity for the participants to monitor or assess the likelihood of a remote service to behave according to participants' expected rules.

## Annex C - Mapping of Gaia-X concepts with concepts used in EU data regulation

The following table maps the Gaia-X concepts with the concepts used within the different European regulations around data (GDPR and the EU acts on data - DxA):

|**European regulations concepts** | **Gaia-X concepts** |
|------------|-------------|
| data processor in GDPR | data provider |
| data subject in GDPR / user in DxA | data Rights Holder |
| consent in GDPR / permission or authorization in DxA	| data usage agreement |
| recipient in GDPR / DxA | data consumer |

The following diagram details these relationships (in black European regulations concepts, in blue Gaia-X concepts):

![Concept mapping Gaia-X vs EU regulations](figures/Gaia-X_models_Data_Product_vs_EU_Regulations.svg)

*Figure 11.1 - Mapping of Gaia-X concepts with EU data regulation concepts*

## Annex D - Conceptual models instantiation example: the Personal Finance Management example

This example describes the various Gaia-X concepts using the Open Banking scenario of a Personal Finance Management service (PFM) in SaaS mode.

Suppose that the PFM service is proposed by a company called MyPFM to an end user Jane who has bank accounts in several banks: Bank(1), Bank(2)...
MyPFM is using services provided by the banks Bank(i) to get the banking transactions of Jane and then aggregates these bank statements to create Jane’s financial dashboard.

![myPFM Example illustration](figures/Gaia-X_Data_Exchanges_Example_myPFM_informal1.svg)

Jane is the End User and also the Data Rights Holder (as data subject per GDPR).

Bank(i) are Data Providers defining the Data Products (Service Offerings) delivering the banking transactions. They are also Resource Owners for the bank statements, which are Virtual Resources composing the Data Products, and as such are also the Data Producers.
The associated Resource Policies are in fact predefined by the PSD directive from the European Parliament. (note: this is not the way that consent management is currently put in place for DSP2 services - the process described below is adapted to the _Financial and Insurance Data Access_ (FIDA) EU regulation draft issued in June 2023.)

MyPFM is the Data Consumer (Service Consumer) that consumes the data (Data Usage, Service Instances) provided by Bank(i) in order to create a financial dashboard and to offer it to Jane.

MyPFM is also likely consuming other Service Instances from a PaaS Provider in order to run its own code, such as the code implementing the dashboard creation.

Because sensitive personal information is involved, she has to sign a specific “contract” stating precisely how myPFM is authorized to use Jane’s data (e.g., authorized for establishing her financial dashboard), transmission to anybody else is prohibited except maybe for associating a payee and an expense category (i.e. payee X is a grocery, payee Y is a gas station, ...).

Firstly, Jane will communicate her various bank account identifiers (IBAN) to myPFM. myPFM will use the Data Product catalogue to find out the appropriate services from each bank – we will suppose for simplicity’s sake that they are all named GetTransactionFromIBAN. PFM will review each Data Product Description to ensure that it is compatible with their needs. As sensitive personal information is involved, Bank(i) will require a signed agreement from Jane. The agreement template is included in the Data Product Description. (Note: ecosystems will likely have predefined standard consent templates in order to enable automatic and agile processing). myPFM will fill in the template and send it to Jane for her to sign it through a digital identity and digital signature provider trusted by Jane, myPFM and Bank(i).
myPFM and each Bank(i) will then configure the GetTransactionFromIBAN service for Jane  and co-sign the contract. 
myPFM can then request Jane’s transaction data (providing Jane's signed Data Usage Agreement), process this data, and provide the financial dashboard to Jane.


Let us now suppose that myPFM also delivers a loan brokering service. To do so, myPFM provides a credit profile to loan institutions to receive credit proposals that they can rank and forward to Jane. 

![myPFM Loanbroker Example illustration](figures/Gaia-X_Data_Exchanges_Example_myPFM_informal2.svg)

In this case, myPFM is both a Data Consumer from Bank(i) and a Data Provider to loan institutions Lender(i).
If Jane wants to use this service, she will first have to sign a new data usage agreement to authorize myPFM to communicate her credit profile (total income, loan capacity, purpose of the loan, ...) to some loan institutions – this credit profile is anonymous and should not enable identifying Jane.
Then myPFM will query the Data Product Catalogue to find loan institutions providing such online loan services and will review the corresponding Data Product Description to check compliance with Jane's data usage agreement and with myPFM policy. 

With each selected Lender(i), myPFM will negotiate, configure, and sign the service contract. The terms of usage will not include Jane’s data usage agreement because the data transmitted to Lender(i) is anonymized at that stage. The terms of usage will include conditions specific to myPFM and Lender(i) business, for instance: myPFM is not authorized to communicate the credit proposal to other credit institutions for a given duration, myPFM guarantees that to their knowledge Jane is a real customer and not a data aggregator, Lender(i) commits to prepare a proposal within x hours, Lender(i) will pay myPFM some money if their offer is selected...

PFM will then call the getLoanProposal service from Lender(i). At that stage, no data is transmitted except an anonymous request identifier. In order to prepare a credit proposal, Lender(i) will have to get the credit profile associated with that identifier. For that Lender(i) will get, from the catalogue, the description of the getLoanRequestData service provided by myPFM, and Lender(i) will configure it and co-sign it. The terms of usage will still not involve Jane's agreement but should include clauses at least as strong as those in Jane’s primary Data Usage Agreement, for instance, that the data shall be used only for the purpose of establishing a credit proposal and shall be deleted within 30 days if the proposal is not activated. Lender(i) will then get the data from myPFM by activating the getLoanRequestData service. At that stage, PFM acts as a Data Provider and Lender(i) as a Data Consumer. Lender(i) will then prepare the loan proposal and make it available to myPFM.

myPFM will then collect the various credit proposals, review them, and rank them to prepare a recommendation for Jane.

The formal operating model is given below:

![myPFM Example Operating Model](figures/Gaia-X_Data_Exchanges_Example_myPFM.svg)

## Annex E - Service Composition Example

#### Practical multi-cloud/-provider/-service composition example

Figure 11.2 presents a pragmatic and practical multi-cloud, multi-provider and multi-service example (or use case) that reflects the current state of the art and practice in cloud services and computing.

![](figures/Figure_11.2-annexe-_conceptual_model-Multiservices-Multiproviders.drawio_rev.png)
*Figure 11.2 - Practical, service template oriented, multi-cloud/provider/service composition example*

This example represents more concretely what can be readily achieved today with available 
cloud services “deployment and management tools” and cloud services providers. 
The example is in line with the generic service composition model, differences 
are more in how the class diagram is organized and configured, but all key functions and 
blocks are identical and common.
 
The example illustrates how multiple cloud providers are selected to fulfil a user service request 
for a distributed application across multiple hosting infrastructures and providers. The requested 
service in this case requires an independent interconnection service provider to ensure connectivity 
across multiple clouds. The service composition is service-template oriented and relies on the selection 
and configuration of service templates to implement a solution compliant with end users and Gaia-X. 
The suppliers using an orchestrator will adapt the generic implementation plan, and output of the Service Composition Tool, to their solutions. This is achieved through adapted templates (usually in implementation models such as Terraform, Ansible, Heat or others) made available from the pool of available resources. 
