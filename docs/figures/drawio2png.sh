#!/bin/sh

infile=$1
outname="${infile%%.*}"

which yq > /dev/null || (echo 'yq is missing, please install it. pip install yq'; exit 1)
which drawio > /dev/null || (echo 'drawio is missing, please install it: https://github.com/jgraph/drawio-desktop/releases/tag/v14.6.13'; exit 1)

# wget --continue https://github.com/jgraph/drawio-desktop/releases/download/v14.6.13/drawio-amd64-14.6.13.deb
# sudo dpkg -i drawio-amd64-14.6.13.deb
# sudo apt-get --yes -f install

IFS="
"
count=0
for page in `cat "${infile}" | xq -r '.mxfile | if (.diagram | type) == "object" then .diagram else .diagram[] end | .["@name"]'`; do
    outfile="$outname-$page.png"
    echo "$infile -> $outfile"
    drawio --output "$outfile" --transparent --export "$infile" --page-index $count
    count=$(($count+1))
done
