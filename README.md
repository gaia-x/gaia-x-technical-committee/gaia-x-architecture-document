# GAIA-X Architecture Documentation

This repository contains the architecture documentation for Gaia-X.

The repository is organized into the following folders:

## Architecture Document

The Architecture Document describes the current common understanding of the Architecture.

The output is published here <https://gaia-x.gitlab.io/technical-committee/architecture-working-group/architecture-document/>

### Local development

In order to build locally without extra configuration, please install <https://github.com/firecow/gitlab-ci-local> and run `gitlab-ci-local --list` to get the list of possible stages.

To execute all stage, type `gitlab-ci-local`.  
To execute a specific stage, type `gitlab-ci-local <stage_name>`, ex `gitlab-ci-local generate_pdf`

### Local development (advanced)

**Note**: *The latest up-to-date commands are in the [.gitlab-ci.yml](.gitlab-ci.yml) file.*

Generated files are in `public/`

## Architecture Decision Records

Architecture Decision Records (ADR) document important decisions that were proposed and accepted for Gaia-X.

The ADR are listed in in appendix of the Architecture Document.

