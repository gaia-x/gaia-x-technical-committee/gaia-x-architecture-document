# Other Concepts

## Gaia-X and Data Meshes

**Data meshes** have emerged recently (since 2018) as an answer to the increasing difficulties of (logically) centralized and (conceptually) monolithic cloud data warehouses, data lakes, data lakehouses and other typically cloud-based approaches to the enterprise-wide management of analytical data to scale and to provide the required flexibility in today's highly dynamic and considerably complex business environments.

This section briefly outlines how Gaia-X incorporates several foundational principles of data meshes, where Gaia-X transcends the narrow boundaries of a data mesh approach, and why Gaia-X may also be characterized as a **mesh of service meshes**.

### Data Mesh Definition

The definition provided by the initial promoter of this concept, Zhamak Dehghani (Dehghani 2022), is widely accepted, accurate, and highly useful (Machado *et al.* 2022, BITKOM 2022):

	A data mesh is a decentralized socio-technical approach to sharing and managing analytical data in complex and large-scale environments within or across organizations.

"Socio-technical approach" indicates that organizations need to adapt their processes and governance in addition to providing the right technology to successfully implement a data mesh. *Sharing*, for obvious reasons, also includes all required capabilities to *access* the data. Originally, data meshes almost exclusively focused on **analytical data** used to support enterprise decision-making as opposed to directly undergirding operational and transactional data management happening in the various IT systems (e.g., ERP, CRM, PDM, MES,...). Recent trends, though, including concepts such as *data hubs* and *data fabrics*, try to overcome this limitation towards a more holistic approach to enterprise data management.

Data meshes closely observe and implement the following four principles:

Principle | Explanation
--------- | -----------
**domain ownership** | (Analytical) data shall be *owned* by different cross-functional (as opposed to specialized!) teams organized around and within appropriate business domains similar to domain-driven design (Evans 2004).
**data as a product** | (Analytical) data shall be managed and treated like a true product which is *produced* by someone in order to provide a well-defined *value* to its *consumers*.
**self-serve data platform** | to enable the different domain teams by providing domain-agnostic capabilities often needed in a *data as a product* approach such as, for instance, data product life cycle management, pipeline workflow management, data processing frameworks, policy enforcement components, and many others.
**federated computational governance** | Data meshes need an appropriate data governance operating model to balance the autonomy and agility of the individual domains with the global interoperability of the overall data mesh (Dehghani 2022, 8). Typically, this will be accomplished by a cross-functional team composed of domain experts, data platform specialists, and other suitable subject matter experts (legal, security, compliance) complemented by heavy automation (e.g., *policies as code*, test and monitoring automation)

### Gaia-X as (Higher Order) "Service Mesh"

Gaia-X itself incorporates several principles present in a data mesh with the marked (and defining) difference that Gaia-X relies on a **service-oriented approach** at the centre of its conceptual model as opposed to restricting itself to analytical data. This generalization notwithstanding, Gaia-X co-opts several usability characteristics of *data as a product* from the data mesh approach such as

- discoverability and addressability of services (e.g., Federated Catalogue, self-sovereign identities)
- providing trustworthy and truthful services through its Trust Framework
- interoperability and composability of services
- guaranteeing secure consumption of services through automated policies (*policies as code*), monitoring, and the Gaia-X Data Exchange Services

Gaia-X Federation Services and the Trust Framework provide capabilities similar to the *self-service data platform* of a data mesh. Also corresponding to the data mesh philosophy, there exists a dedicated domain-independent organizational unit to operate and manage this platform as well, the Federator. Yet, in contrast to the smaller focus of data meshes, the Gaia-X Ecosystem is designed *ex-ante* for federating independent autonomous ecosystems itself (cf. the three Gaia-X *planes* in section Gaia-X ecosystems).

Like with data meshes, the organizational structure of Gaia-X itself (and also Gaia-X-based ecosystems) also relies on a substantially federated governance model when considering the Gaia-X Association (or Gaia-X ecosystem federators), its Committees, Working Groups, and decision processes. In the case of Gaia-X (or Gaia-X based ecosystems), though, all actors in the various governance functions belong to different legal entities, unlike typical data mesh situations where they appertain to a single organization or enterprise.

The term "domain" in data mesh implementations characteristically denotes "bounded contexts" such as spheres of knowledge, influence, activities, or responsibilities *within* a potentially large single organization (Evans 2004, Deghgani 2022). At face value, its Gaia-X equivalent, `Participant`, possesses almost identical semantics: "A `Participant` is an entity as defined in ISO/IEC 24760-1 as an "item relevant for the purpose of operation of a domain that has recognizably distinct existence". Practically, though, Gaia-X `Participants` will mostly consist of independent legal entities.

In summary, a high degree of similarity and overlap between the principles applied for data meshes and for Gaia-X can be recognized. On the other hand, the deviations of Gaia-X with respect to the data mesh approach identified above amount to a form of conceptual generalization. Whereas data meshes primarily concentrate on the management of (analytical and other) data for a single (albeit large and complex) organization, typically a legal entity, Gaia-X provides organizational standards and technical components for realizing whole ecosystems consisting of several independent organizations. Extending this line of thinking one may be even (rightfully) enticed to designate Gaia-X as a **mesh of service meshes** (not just a mesh of data meshes). Note, however, that this particular usage of the term "service mesh" is different from the one encountered in microservice architectures[^service-mesh-classical].

[^service-mesh-classical]: In (typically container-based) microservices architectures a service mesh provides software components and mechanisms to separate cross-cutting concerns of service-to-service communications from the business logic of the individual microservices into a so-called “control plane”. This segregation simplifies the development, operations, and management of larger microservices environments. It is achieved by making every microservice communicate with another one (the so-called east/west traffic) over dedicated components called proxies. See, for instance, [Tech Radar - Service Mesh](https://techradar.softwareag.com/technology/service-meshes/).

## Computational Contracts

The Gaia-X Association is not getting involved in the realisation of the `Contract`. However, to ease participants with the establishment and to enter into a contractual relationship, we are defining below a common model for `Contract`.

### Concept: Computable Contracts as a service

- Contracts are the basis for business relationships.
- Whereas a licensor has rights with respect to a resource and is willing to (sub-)license such rights by a defined set of conditions.
- Whereas a licensee would like to get license rights with respect to a resource by a defined set of conditions.
- Licensor and licensee agree on it in the form of a contract.
- Every role of the Gaia-X Conceptual Model as well as of the Operational model can be seen as legal persons and therefore may have a role as a licensor or licensee or both.
- In traditional centralized ecosystems the platform provider which is very often the ecosystem owner, defines the contractual framework and participants need to accept without any possibility for negotiation.
- In distributed and federated ecosystems individual contracting becomes much more important to support individual content of contractual relations, e.g., individual sets of conditions.
- The ability to negotiate on contracts is key for sovereign participation. The ability to observe if all parties of a contract behave the way it is agreed, to validate their rights, and to fulfil their obligations and ensure that no one can misuse information is key for a trustful relationship.
- Computable contracts aim to ease the complex processes of contract design, contract negotiation, contract signing, and contract termination as well as to observe the fulfilment of contractual obligations and compliance with national law.

```mermaid
flowchart TB
participant[Legal Person]
op_role[Operational Model roles]
cm_role[Conceptual Model roles]
compliance[Gaia-X Compliance]
licensor[Licensor]
licensee[Licensee]
lic_rights[License rights]
usage[Gaia-X resource usage]
t_c[Terms & Conditions]
contract[Contract]
cc_elts[Computable Contract Elements]
cc[Computable Contract]
ncc[Non computable Contract]

op_role & cm_role -- defines --> participant

participant -- is a --> licensee & licensor

licensee -- requests --> lic_rights
licensor -- owns --> lic_rights

licensee -- accepts --> contract
licensor -- offers --> contract

lic_rights -- determines --> usage


lic_rights -- sub-licensed_by --> t_c

t_c -- defined by --> contract

contract -- represented by --> ncc & cc

cc -- consists of --> cc_elts

cc_elts -- verified by --> compliance
participant -- verified by --> compliance
```

## Trusted execution of services

**Trusted Execution Environments** (TEEs) provide a secure enclave within a computing system where sensitive operations can be executed with a high degree of confidentiality and integrity. These environments offer a protected space that is isolated from the rest of the system, shielding it from unauthorized access or tampering. TEEs are designed to safeguard critical processes, such as cryptographic operations, secure key storage, and sensitive data processing, making them vital components in ensuring the security of modern computing platforms.

In the Gaia-X context, TEE can be used to increase the trust in the software running on a clearing house. Solutions like Intel SGX (r) allows to run software with the guarantee that the source code nor binary have been tampered since they were first built. Data processed in an enclave are also invisible from the eye of an external actor, as the enclave is obfuscated from the rest of the machine.

Running components on SGX or other TEE systems allow to trust instances without having to rely solely on the Gaia-X AISBL to identify the trusted providers and communicate a list. It also allow the network of Clearing Houses provider to grow without diminishing the confidence in the issued compliance VerifiableCredentials.