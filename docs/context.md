# Gaia-X Vision: Enable trusted  decentralized digital ecosystems  

The term **“Digital Ecosystem”** in this document is defined as a "group of interacting participants which have agreed through a formal governance body to a set of **“Policy Rules”** that any participant needs to conform to"; Usually Policy Rules include a list of attributes, ecosystem conformity criteria, the method of verification by a mutually agreed trusted party and the method of conformity attestation. Digital ecosystems can be build around Infrastructure-, Application-, Data-  and any combination of those services.

The Gaia-X architecture describes how to support the translation of Policy Rules and their validation and verification in the **digital** realm.

A fundamental design principle is to support a **decentralized** setup. This is to allow each ecosystem to maintain control over their definiton scope, whilst adherance to a common architecture maintains interoperability and allows the setup of trust federations between ecosystems.

## Main Capabilities

This document describes the Gaia-X **Trust Framework** which adresses three main pillars

- for digital ecosystems:
  - the validation and verification of conformity provided towards agreed criteria ("ecosystem compliance")
  - the validation and verification of ecosystem enabling ("Federation") services against the criteria defined by the ecosystem governance authority. Participants can rely on enabling and federation services to comply with ecosystem rules.

- for individual parties:
In order to self determine on how and when others are using a participants data and services, the trust framework allows:
  - to collect credentials validating compliance of participants, services and data product attributes to rules of different ecosystems
  - verification of such credentials at the time of negotiation between two participants
  
- Interoperability between digital ecosystems by allowing individual ecosystems to agree on common ontologies and Trust Anchors (ref. chapter 3.2.1 for definition). The Trust Framework includes extension and delegation mechanisms.

Providers of Trust Framework services  are referred to as "Trust Service Providers" in this document.

### Trust Anchors, Trust Service Providers, Credentials, Wallets and Federated Trust

As there are diverging definitions of these terms we introduce the terms for this document as follows:

- **Trust  Anchors** are the authoritative entity for which trust for a given scope is assumed and not derived. A trust anchor defines the scope, format, compliance rules and the accreditation criteria for **Trust Service Providers**.

(For the scope of Gaia-X credentials, the AISBL is the Trust Anchor. It maintains the Trust Anchor Certificate and the list of TSP per scope (trusted-issuers) in the Gaia-X registry. Similarly, the European Commission maintains the authoritative List of Trusted Lists (LOTL) with eIDAS conformant TSP,...)

- A **Trust Service Provider** fullfills all criteria set by the **Trust Anchor** for the issuance of **Credentials**; In the scope of Gaia-X the Gaia-X digital clearinghouses are the TSP for the scope of issuance of Gaia-X credentials. The TSP listed in the "Trusted Lists" for a given scope will issue credentials or certificates for that scope. Multiple credentials may be combined into a verifiable presentation.

- **Participants**  may collect verifiable presentations and  -credentials issued from multiple **Trust Service Providers** (e.g. an ID Credential, membership credentials, compliance credentials...) and store them in a wallet. The verifiable presentations and -credentials can be verified (e.g. checked for revocation) at any time.

- **Federation of Trust** an ecosystem can decide to accept credentials from  TSP accredited by multiple Trust Anchors for a given scope (e.g. the Gaia-X AISBL maintains a list of Trust Anchors (and their TSP) from different regions which provide digital IDs)

### Ecosystem view

Usually, digital ecosystems are no islands but they exist in a specific context

- Regional: Participants have to ensure compliance to the regional regulations and legal requirements
- Domain: Industry specific standards and regulations exist
- Sub-Domain: On top of domain-specific standards, specific industries/sub-domains require specific standards
- Commercial Context: The interactions may be constrained by specific commercial agreements

The Gaia-X Trust Framework provides an

- extensible ontology (The Gaia-X Schema) which allows ecosystems to describe services:
  - clearly attributing the providers validated identity
  - providing a descripion of the service and its attributes
- validation and verification schema for individual attributes
- a mechanism to validate compliance to the agreed criteria defined in the policy rules of the ecosystem

#### Automated on- and offboarding of ecosystem participants

Validation of all attributes against the conformity scheme/s of a given ecosystem allows the automated onboarding of a participant. Ecosystems can decide to rely for the validation of attributes on services provided by external trusted sources.

The same mechanism can be used to identify participants who are not any more compliant and provide automated offboarding.

#### Trusted operating environment for decentralized enabling services

Ecosystems typically provide a set of enabling services (like catalogues, identity and access management solutions, portals and marketplaces...). Validation of compliance of these services using the Gaia-X Trust Framework provides an environment the ecosystem participants can trust.

#### Federation of ecosystems

Ecosystems can agree on common trust anchors, trust service providers, ontologies, and shared services based on individual agreements. This allows a high level of interoperability between ecosystems and for participants in multiple ecosystems.

Providers which are cooperating to provide interoperable services can agree on a common set of criteria, which they can use to create offering specific "labels" (e.g. an infrastructure or middleware platform which has been validated for interopability aross different providers)

![Federation of Ecosystems](docs/figures/D3E_Ecosystem_Federation.png)

### Participant view

#### Services compliant to defined criteria

The Gaia-X Policy Rules Commmittee has defined a set of criteria for (as of today mostly) European Users of Cloud Services and Data Sharing. This gives users a transparency over the attributes (location, compliance to permissable standards, self-declaration of criteria defined relevant by the PRC members). It helps users to select services based on regulatory compliance, e.g. GDPR, Cybersecurity, Portability and Interoperability.

#### Sovereign Data Sharing

Individual negotiation of the conditions of Data Sharing between two participants relies on the declaration of specific attributes

Gaia-X defines with the "Data Product Conceputal Model" a set of attributes which allow such negotiation ensuring alignment with the CEN/CENELEC Trusted Data Transactions and adherance to the regulatory requirements in the EU (e.g. Data Act, GDPR...).

The Gaia-X Trust Framework allows the validation and verification of such attributes (and of services which provide such attestation)

Attribute Validation can be used also for attribute (ODRL property value) validation as e.g. used in the Dataspace Protocol.

![Decentralized Ecosystems](docs/figures/D3E_Decentralized_Ecosystems.svg)

Participants are usually member in multiple ecosystems. Gaia-X provides the mechanisms that credentials issued to the participant can be used interoperably across ecosystems implementing the Gaia-X architecture based on agreemements of trust between the ecosystems.

< Picture to show different Properties: Policy Rules by ecosystem | Data Attributes | Individual ODRL profiles >

The Gaia-X Architecture supports digital processes

- For ecosystems to define and implement digital compliance schemes for participants, services, resources & data exchange
- For participants to provide proof of conformity to sets of criteria defined in the policy rules of the ecosystems they elect to participate in  
- For participants to present and verify to each other the proof of compliance to specific claims to support individual transactions (e.g. verification of Data Product attributes like DUA when sharing  data)
- For an ecosystem to ensure that foundational services which support participant interaction are compliant with the ecosystem policy rules

## Architecture Foundation

Participants will typically be part of a number of ecosystems which may have overlapping Policy Rules, e.g.

- Each ecosystem requires participant identification (for natural persons and/or legal entities)
- Regulation requires participants in a specific geography to adhere to regional regulations
- Industry sectors may add specific regulation, compliance to technology standards etc.

 <Picture of overlapping ecosystems e.g. hierarchical planes + fan out by sector>

To provide an interoperable mechanism for proof of compliance to the individual ecosystems the Gaia-X architecture builds on top of W3C Standards for decentralized systems:

- W3C Resource Description Framework (**RDF**) for a common semantic basis
  - represents the policies as triples (subject, predicate, object)
  - JSON path to evaluate the credentials
  - SPARQL to query the RDF triples.
- W3C Decentralized Identifiers (**DID**) (for natural persons, legal entities and devices)
  - Allows mapping of individual ID Schemes across ecosystems
- W3C Verifiable Credentials (**VC**)
  - Verifiable Credentials Data Model v2.0
  - JWT Specification
- W3C Open Digital Rights Language (**ODRL**)
  - allows to express policies that can be evaluated for conformity validation or in contract negotiation between participants
- W3C Data Catalog Vocabulary (**DCAT**)
  - allows publication and discovery of catalogues with defined vocabularies

## Gaia-X Trust Framework

Implementation of the Gaia-X Trust Framework ensures that all ecosystem participants are conforming to the policy rules agreed between the participants of the ecosystem. It provides the means to define the rules in a semantically interoperable format **(RDF)**, to provide decentralized validation and verification **(VC)** of individual claims through trusted parties aligning with CASCO mechanisms and provides a preocess to automatically enforce ecosysetem rules.

 <VC picture: Registry with Schema, Trust Anchors, Issue, Holder, Verifier

### Proof of conformity to CASCO standards

The general process to provide proof of conformity is defined in ISO / CASCO; Gaia-X provides the mechanisms to translate these processes and the model of attestations by Conformity Assessment Bodies into the digital equivalent.

Examples:

- Conformity to Gaia-X Policy Rules through permissible standards (e.g. ISO/IEC 27001, CISPE, EU Cloud CoC, SWIPO, BSI C5,SecNumCloud,TISAX, CSA CCM...)
- Conformity to ecosystem Rules (e.g. validation of compliance to specific Domain Regulation by Conformity Assessment Bodies)
- Verification of ecosystem Membership (e.g. Service provided by an Associaton)

### Proof or conformity to specific Properties

Attributes (as they are defined in the Gaia-X or ecosystem shema) may rely for verification on services the ecosystem has agreed upon. These can be existing Identity and Access Management Solutions or most generically "Policy Information Points" operated as service trusted by the ecosystem governance authority.  Gaia-X provides the mechanism to verify conformity of such services to the rules of the ecosystem governance.

Examples:

- Verification Data Usage Agreements
- Verification of adherence to a Technical Specification by an automated validation of a Standards body…

## **Gaia-X Trust Framework Compliance Service Design**

It defines the process of going through and validating the set of automatically enforceable rules to achieve the minimum level of compatibility in terms of:

- syntactic correctness
- schema validity
- cryptographic signature validation
- attribute value consistency
- attribute value verification

The claims are all contained in verifiable credentials, independently of who is performing the assessment. The ICAM document describes the format to allow  an envelope of such claims and verified credentials. Claims validation can be done either by using publicly available open data and performing tests or using data from [Trusted Data Sources](component_details.md#gaia-x-trusted-data-sources-and-gaia-x-notaries) as defined in the following sections.

The underlying trust is then provided by provided by [Ecosystem Credentials](operating_model.md#gaia-x-credentials-and-attestations).

## Service Characteristics

- This section describes the core principles to build Gaia-X Schemes
- It describes the mechanism how to create schema automatically based on LinkML and defines how an ecosystem can use it to easily create and expand existing schemes
- It provides the Ontology for services that are used for the operationalization of digital ecosystems…

## ICAM

- Enveloped trust, Trust Delegation
- OID4VCI experimental for GXDCH

## Data Exchange Services

- Defines the Data Product Conceptual and Operational Model enabling  sovereign data access and usage: the data rights holders control how, why, by whom and when their data are used. This provides, in particular, means to prove conformity to EU regulation and controls regarding data (GDPR, Data Act,...).
- Defines the basic set of data exchange services which allow multiple interoperable implementations

## Software Components

- Distributed Registry: Holds the referential for ecosystem Schema and Trust Anchors  
- Compliance service: Provides ODRL-based policy reasoning to evaluate ecosystem conformity and issues ecosystem Verifiable Credentials
- Notary (registryNumber notarization API): verifies company registration numbers
- Credential Event Service (CES): Allows synchronization of credentials across decentralized and distributed registries

### Enabling Services

Enabling services facilitate the setup of ecosystems, providing functionalities that allow publication and discovery, validation and verification, trusted policy information points and participant access and onboarding (portal)

#### Validation and Verification Services

Allows each ecosystem to define operators who can issue and verify conformity credentials for the criteria defined following the ecosystem Policies Rules. Using the Trust Anchor Credentials defined in the Architecture and ICAM documents this function can be delegated

#### Policy Information Points

Policy Information Points evaluate a specific set of rules and can be used as input to both the Validation and Verification services as well as any policy-based reasoning (e.g. by the Dataspace Protocol). A specific type of PIP is the Identity and Access Management system agreed by the ecosystem. This includes the establishment of SSI solutions.

#### Federated Catalogue Services

In order to discover the agreed references (e.g. Vocabularies, Data Models,…) as well as to publish available services the ecosystem often provides a controlled publication by trusted operators

#### Auditing / Observability Services

#### Portals

## Operating Model

## GXDCH

## Gaia-X as a member of the Data Spaces Business Alliance ("DSBA")

Gaia-X together with the [International Data Spaces Association (IDSA)](https://internationaldataspaces.org/), the [FIWARE Foundation](https://www.fiware.org/) and the [Big Data Value Association](https://bdva.eu/) has formed the [Data Spaces Business Alliance](https://data-spaces-business-alliance.eu/) to provide a comprehensive and complementary stack of specifications and OSS components that enable and create value out of distributed digital (data) ecosystems.

![Gaia-X in the context of an OpenDEI Data Space](figures/DSBA_Deliverables_Stack.png)

The DSBA members continue to converge and integrate their respective artefacts to provide a comprehensive, interoperable and scalable set of solutions, but they remain open to the integration of other solutions.
